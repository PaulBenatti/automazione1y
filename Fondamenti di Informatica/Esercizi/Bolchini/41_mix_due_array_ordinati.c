#include <stdio.h>
#define LMAX 5

int main(int argc, char * argv[])
{
	int seq[LMAX*2], seq_due[LMAX];
	int tmp, dim, i, j, minPos;
	for (i = 0; i < LMAX; i++) {
		scanf("%d", &seq[i]);
	}
	printf("Ora la seconda stringa\n");
	for (i = 0; i < LMAX; i++) {
		scanf("%d", &seq_due[i]);
	}
	dim = LMAX * 2;
	for (i = LMAX; i < dim; i++) {
		seq[i] = seq_due[i - LMAX];
	}

	/* Toglie i doppioni */
	for (i = 0; i < (dim - 1); i++) {
		j = i + 1;
        while (j < dim) {
            /* If any duplicate found */
            if (seq[i] == seq[j]) {
                /* Delete the current duplicate element */
                for (tmp = j; tmp < dim; tmp++) {
                    seq[tmp] = seq[tmp + 1];
                }
                /* Decrement dim after removing duplicate element */
                dim--;
                /* If shifting of elements occur then don't increment j */
            } else {
				j++;
			}
        }
    }

	/* Ordina l'array */
	for (i = 0; i < dim - 1; i++) {
		minPos = i;
		for (j = i + 1; j < dim; j++) {
			if (seq[j] < seq[minPos]) {
				minPos = j;
			}
		}
		tmp = seq[i];
		seq[i] = seq[minPos];
		seq[minPos] = tmp;
	}
	for (i = 0; i < dim; i++) {
		printf("%d ", seq[i]);
	}
	printf("\n");
	return 0;
}
