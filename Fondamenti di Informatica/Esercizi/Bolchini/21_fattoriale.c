#include <stdio.h>

int main(int argc, char * argv[])
{
    int val, i, ris;
	do {
		scanf("%d", &val);
	} while (val <= 0);
	ris = 1;
	/*for (i = 2; i <= val; i++) {
		ris = ris * i;
	}*/
	for (i = val; i > 1; i--) {
		ris = ris * i;
	}
	printf("%d\n", ris);
    return 0;
}