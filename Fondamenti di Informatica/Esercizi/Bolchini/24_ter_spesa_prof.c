#include <stdio.h>
#define NMAX 50
#define STOP 0

int main(int argc, char * argv[])
{
    float val[NMAX], prezzo, costo_totale, max;
    int i, dim, num_pezzi;
    
    dim = 0;
    scanf("%f", &prezzo);
    while(prezzo >= STOP && dim < NMAX){
        val[dim] = prezzo;
        dim++;
        scanf("%f", &prezzo);
    }
	do {
		printf("Ora il valore massimo maggiore o uguale a zero\n");
		scanf("%f", &max);
	} while (max < 0);
	
	costo_totale = 0;
	num_pezzi = 0;
	
	for (i= 0; i < dim; i++) {
		prezzo = val[i];
		if (costo_totale + prezzo <= max) {
			costo_totale += prezzo;
			num_pezzi++;
		}
	}
	
    printf("%d: %f\n", num_pezzi, costo_totale);
    return 0;
}
