/* Sottoproigramma verifiche che ricevuta una stringa che contiene una password restituisce 1 se sono verificate le seguenti condizioni, 0 altrimenti: */
/* Almeno una cifra, un carattere speciale, lunghezza minimo 8, non contiene 2 caratteri consecutivi */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>

int verifica (int, int, int, int, char []);
int numbercheck (char []);
int specialcheck (char []);
int repeatcheck (char []);
void print_usage (void);

int main(int argc, char * argv[])
{
	char *password;
	int ris, number, special, lenght, repeat, next_option;
	const char* short_options = "nsl:rp:";
	const struct option long_options[] = {
		{"number",	0, NULL, 'n'},
		{"special",	0, NULL, 's'},
		{"lenght",	1, NULL, 'l'},
		{"repeat",	0, NULL, 'r'},
		{"password",1, NULL, 'p'},
		{NULL,		0, NULL, 0	}
	};

	ris = 0;
	number = 0;
	special = 0;
	lenght = 0;
	repeat = 0;
	password = NULL;

	while ((next_option = getopt_long (argc, argv, short_options, long_options, NULL)) != -1 && ris != -1) {
		if (next_option == 'n') {
			number = 1;
		} else if (next_option == 's') {
			special = 1;
		} else if (next_option == 'l') {
			lenght = atoi (optarg);
		} else if (next_option == 'r') {
			repeat = 1;
		} else if (next_option == 'p') {
			password = optarg;
		} else {
			ris = -1;
		}
	}
	
	if (password && ris != -1) {
		ris = verifica (number, special, lenght, repeat, password);
		if (ris) {
			printf ("La password è valida\n");
		} else {
			printf ("La password non è valida\n");
		}
	} else {
		print_usage ();
	}

	return 0;
}

int verifica (int number, int special, int lenght, int repeat, char seq[])
{
	int isok, len, hasnumber, hasspecial, hasrepeat;

	len = 1;
	hasnumber = 1;
	hasspecial = 1;
	hasrepeat = 0;

	if (lenght) {
		if ((len = strlen (seq)) < lenght) {
			printf ("La password non e' abbastanza lunga\n");
			len = 0;
		} else {
			len = 1;
		}
	}
	if (number) {
		hasnumber = numbercheck (seq);
		if (!hasnumber) {
			printf ("La password non ha cifre\n");
		}
	}
	if (special) {
		hasspecial = specialcheck (seq);
		if (!hasspecial) {
			printf ("La password non ha caratteri speciali\n");
		}
	}
	if (repeat) {
		hasrepeat = repeatcheck (seq);
		if (hasrepeat) {
			printf ("La password ha due caratteri consecutivi uguali\n");
		}
	}
	
	isok = hasnumber && hasspecial && !hasrepeat && len;

	return isok;
}

int numbercheck (char seq[])
{
	int i, ris;

	ris = 0;
	for (i = 0; seq[i] != '\0' && !ris; i++) {
		if (isdigit (seq [i])) {
			ris = 1;
		}
	}

	return ris;
}

int specialcheck (char seq[])
{
	int i, ris;

	ris = 0;
	for (i = 0; seq[i] != '\0' && !ris; i++) {
		if (!isalnum /* In un controllo normale ci sarebbe ispunct ma qui la specifica diceva che gli spazi valgono come caratteri speciali */ (seq [i])) {
			ris = 1;
		}
	}

	return ris;
}

int repeatcheck (char seq[])
{
	int i, ris;

	ris = 0;
	for (i = 0; seq[i] != '\0' && !ris; i++) {
		if (seq[i] == seq[i+1]) {
			ris = 1;
		}
	}

	return ris;
}

void print_usage (void)
{
	printf ("Non hai utilizzato il programma correttamente o hai richiesto la pagina di aiuto, ecco come fare:\n");
	printf (
		"-n	--number	controlla se nella password c'è almeno un numero\n"
		"-s	--special	controlla se nella password c'è almeno un carattere speciale, compresi spazi e tab\n"
		"-l	--lenght	specifica la lunghezza minima della password (inseriscila subito dopo aver richiamato questo argomento)\n"
		"-r	--repeat	controlla se nella password non ci sono caratteri ripetuti consecutivi\n"
		"-p	--password	questo argomento e' obbligatorio, dopo averlo richiamato devi scrivere la password da controllare\n"
	);
	printf ("qualsiasi altra cosa richiama questa pagina\n");

	return;
}
