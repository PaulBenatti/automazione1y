#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define OPZIONI 3

int * contaCaratteri (char [], char[]);

int main (int argc, char * argv[])
{
	char *seq, *lookup;
	int *contatori;
	int i;

	if (argc == OPZIONI) {
		seq = argv[1];
		lookup = argv[2];
		if ((contatori = contaCaratteri (seq, lookup))) {
			for (i = 0; lookup[i] != '\0'; i++) {
				printf ("%c: %d\n", lookup[i], contatori[i]);
			}
			free (contatori);
		} else {
			printf ("contaCaratteri: Errore allocazione memoria\n");
		}
	} else if (argc < OPZIONI) {
		printf ("Error: too few arguments\n");
	} else {
		printf ("Error: too many arguments\n");
	}

	return 0;
}

int * contaCaratteri (char seq[], char lookup[])
{
	int *contatori;
	int dim, i, j;

	dim = strlen (lookup);
	if ((contatori = malloc (dim * sizeof (int)))) {
		for (i = 0; i < dim; i++) {
			contatori[i] = 0;
		}
		for (i = 0; seq[i] != '\0'; i++) {
			for (j = 0; seq[i] != lookup[j]; j++) {
				;
			}
			contatori[j]++;
		}
	} else {
		contatori = NULL;
	}

	return contatori;
}
