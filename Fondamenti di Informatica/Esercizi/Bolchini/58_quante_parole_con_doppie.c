#include <stdio.h>
#include <string.h>
#define NMAX 1000

int conta (char [], int);

int main(int argc, char * argv[])
{
	char seq[NMAX];
	int dim, ris;

	gets(seq);

	ris = conta (seq, dim);

	printf("%d\n", ris);
	return 0;
}

int conta (char seq[], int dim)
{
	int i, ris;
	
	ris = 0;
	for (i = 0; seq[i] != '\0'; i++) {
		if (seq[i] == seq[i+1]) {
			ris++;
			while (seq[i] != ' ') {
				i++;
			}
		}
	}
	return ris;
}
