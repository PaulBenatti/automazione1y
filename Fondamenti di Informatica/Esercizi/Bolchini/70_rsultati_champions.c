/* Prende un file con una stringa e 6 numeri per quattro volte. Restituisce un file con la stringa e la somma di quei numeri */
#include <stdio.h>
#define INPUT "./dati.txt"
#define OUTPUT "./out.txt"
#define PARTITE 6
#define CMAX 50

int main(int argc, char * argv[])
{
	FILE *fin, *fout;
	char nome[CMAX+1];
	int punti, i, tmp;

	if (fin = fopen (INPUT, "r")) {
		if (fout = fopen (OUTPUT, "w")) {
			while (fscanf(fin, "%s", nome) != EOF) { /* Vuol dire: per ogni stringa prendi le 6 cose dopo, salvale una alla volta in una variabile intera e sommale */
				punti = 0;
				for (i = 0; i < PARTITE; i++) {
					fscanf (fin, "%d", &tmp);
					punti += tmp;
				}
				fprintf (fout, "%s %d\n", nome, punti);
			}
			fclose (fout);
		} else {
			printf ("Problemi con il file %s\n", OUTPUT);
		}
		fclose (fin);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}
