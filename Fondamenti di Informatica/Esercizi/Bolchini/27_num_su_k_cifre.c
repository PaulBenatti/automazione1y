#include <stdio.h>
#define BASE 10

int main(int argc, char * argv[])
{
	int num, k, tmp, cifre;
	do {
		scanf("%d", &num);
	} while (num <= 0);
	do {
		scanf("%d", &k);
	} while (k <= 0);
	tmp = num;
	for (cifre = 0; tmp > 0; cifre++) {
		tmp = tmp / BASE;
	}
	for (tmp = 0; tmp < (k - cifre); tmp++) {
		printf("0");
	}
	printf("%d\n", num);
	return 0;
}