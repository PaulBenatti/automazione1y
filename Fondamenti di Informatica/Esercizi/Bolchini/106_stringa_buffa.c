/* Scrivere un programma che acquisisce da riga di comando una stringa e visualizza 1 se la stringa `e buffa 0
altrimenti.
Per determinare se una stringa sia buffa, si proceda nel seguente modo. Si calcoli la stringa opposta (ovvero
se la stringa inziale `e abc si calcola cba). Si procede poi per ciascuna stringa, calcolando le differenze in
valore assoluto tra due caratteri adiacenti (ovvero per la prima stringa la differenza tra a e b, la differenza
tra b e c e in modo analogo per la seconda). Se la sequenza delle differenze in valore assoluto `e la stessa
per entrambe le stringhe la stringa `e buffa. Per esempio, la stringa acxz `e buffa, la stringa ivvkx non lo
è. */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define OPZIONI 2

void invertiSeq (char [], char [], int);

int main (int argc, char * argv[])
{
	char *nomeExe, *seq, *seq_inv;
	char buff[2], buff_inv[2]; /* Anche questi potevo non usarli bastava fare diff = seq[i] - seq_inv[i] */
	int diff, diff_inv, dim, i, ris;

	nomeExe = argv[0];
	if (argc == OPZIONI) {
		seq = argv[1];
		dim = strlen (seq);
		if ((seq_inv = malloc ((dim + 1) * sizeof (char)))) {
			ris = 1;
			invertiSeq (seq, seq_inv, dim); /* Potevo non calcolarla neanche facendo diff_inv = seq[dim - 2 - i] - seq [dim - 2 - (i + 1)] */
			/* Il -2 viene dal fatto che -1 è il \n e -1 è il fatto che ti devi fermare al penultimo così esiste anche il successivo da sottrarre */
			buff[0] = seq[0];
			buff_inv[0] = seq_inv[0];
			for (i = 1; i < dim && ris; i++) {
				buff[1] = seq[i];
				buff_inv[1] = seq_inv[i];
				diff = buff[0] - buff[1];
				diff_inv = buff_inv[0] - buff_inv[1];
				if (abs(diff) != abs(diff_inv)) {
					ris = 0;
				}
				buff[0] = buff[1];
				buff_inv[0] = buff_inv[1];
			}
			printf ("%d\n", ris);
		} else {
			printf ("%s: Errore nell'allocazione memoria per %d caratteri\n", nomeExe, (dim + 1));
		}
	} else if (argc < OPZIONI) {
		printf ("%s: too few arguments\n", nomeExe);
	} else {
		printf ("%s: too many arguments\n", nomeExe);
	}

	return 0;
}

void invertiSeq (char source[], char target[], int dim)
{
	int i, j;

	i = dim - 1;
	for (j = 0; j < dim; j++) {
		target[i] = source[j];
		i--;
	}
	target[dim] = '\0';

	return;
}
