/* Prende tutti i numeri primi presenti in un file e li stampa in un altro */
#include <stdio.h>
#include <stdlib.h>
#define INPUT "./input.txt"
#define OUTPUT "./primi.txt"

int * primiDaFile (FILE*, int*);

int primo (int);

int main (int argc, char * argv[])
{
	FILE *fin, *fout;
	int *ris;
	int dim, i;

	if ((fin = fopen (INPUT, "r"))) {
		if ((fout = fopen (OUTPUT, "w"))) {
			ris = primiDaFile (fin, &dim);
			if (ris) {
				for (i = 0; i < dim; i++) {
					fprintf (fout, "%d\n", *(ris + i));
				}
			}
			free (ris);
			fclose (fout);
		} else {
			printf ("Problemi con il file %s\n", OUTPUT);
		}
		fclose (fin);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}


int * primiDaFile (FILE * nomeFile, int * dim /* In realtà serve per tornare il numero di elementi da mettere nel nuovo file, perché non essendo una stringa non c'è il terminatore */)
{
	int size, curN, j;
	int *valori;

	size = 0;
	while (fscanf (nomeFile, "%d", &curN) != EOF) {
		if (primo (curN)) {
			size++;
		}
	}

	rewind (nomeFile); /* Torna all'inizio del file */	
	if ((valori = malloc (size * sizeof (int)))) {
		j = 0;
		while (fscanf (nomeFile, "%d", &curN) != EOF) {
			if (primo (curN)) {
				*(valori + j) = curN;
				j++;
			}
		}
	} else {
		printf ("Errore allocazione memoria per %d numeri primi da salvare", size);
	}

	*dim = size;
	return valori;
}

int primo (int num)
{
	int eprimo, divisore;
	
	if (num == 1 || (num % 2 == 0 && num != 2)) {
		eprimo = 0;
	} else {
		eprimo = 1;
		for (divisore = 3; divisore <= (num / 2) && eprimo; divisore = divisore + 2) {
			if (num % divisore == 0) {
				eprimo = 0;
			}
		}
	}
	return eprimo;
}

