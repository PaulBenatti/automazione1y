#include <stdio.h>
#define NVAL 10

typedef struct occ_s {
	int valore;
	int cont;
} occ_t;

int main(int argc, char * argv[])
{
	occ_t numeri[NVAL];
	int num; /* il valore fornito dall'utente */
	int numcontatori; /* il numero di valori diversi che sto contando effettivamente */
	int i, j; /* indici per scandire l'array */
	int trovato; /* variabile sentinella per quando cerco un valore tra quelli gia' incontrati */

	scanf("%d", &num);
	numeri[0].valore = num;  	/* il primo numero senz'altro non l'ho gia' visto */
	numeri[0].cont = 1;			/* l'ho visto 1 volta */
	numcontatori = 1;  			/* ho visto 1 valore di cui tenere il conteggio */
	for (i = 1; i < NVAL; i++) {
		scanf("%d", &num);
		/* cerco il valore che ho letto tra quelli di cui sto tenendo il conteggio */
		trovato = 0;
		j = 0;
		while (j < numcontatori && !trovato) {
			if (num == numeri[j].valore) {
				trovato = 1;
			} else {
				j++;
			}
		}
		if (trovato) {	/* se l'ho trovato, j e' la sua posizione */
			numeri[j].cont++;
		} else {
			/* numero mai visto prima, lo memorizzo e segno che l'ho visto 1 volta */
			numeri[numcontatori].valore = num;
			numeri[numcontatori].cont = 1;
			numcontatori++;	/* c'e' un valore in piu' di cui tener traccia */
		}
	}
	/* visualizzo tutti i valori conteggiati */
	for (i = 0; i < numcontatori; i++) {
		printf("%d: %d\n", numeri[i].valore, numeri[i].cont);
	}
	return 0;
}
