/* Sottoprogramma che ricevuta in ingresso una stringa ne restituisce una nuova contenente tutte e sole le vocali presenti nella stringa in ingresso */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define CMAXSOLOMAIN 50

char* soloVocali (char []);

int main (int argc, char * argv[])
{
	char seq[CMAXSOLOMAIN+1];
	int addr_ris, i;
	
	gets (seq);

	addr_ris = soloVocali (seq);
	
	if (addr_ris) {
		printf ("%s\n", addr_ris);
		free (addr_ris); /* Va qua nel main per forza, se libero su qualcuno che ha restituito NULL crasha */
	}
	return 0;
}

char* soloVocali (input)
{
	int *addr;
	int i, j, size;

	for (i = 0; input[i] != '\0'; i++) {
		if (input[i] == "a" || input[i] == "e" || input[i] == "i" || input[i] == "o" || input[i] == "u") {
			size++;
		}
	}

	addr = malloc ((size + 1) * sizeof (int));
	if (addr != NULL) {
		j = 0;
		for (i = 0; input[i] != '\0'; i++) {
			if (input[i] == "a" || input[i] == "e" || input[i] == "i" || input[i] == "o" || input[i] == "u") {
				*(addr + j) = input[i];
				j++;
			}
		}
		*(vocali + j) = '\0';
	} else {
		printf ("Errore allocazione memoria per %d interi", dim);
	}

	return addr;
}