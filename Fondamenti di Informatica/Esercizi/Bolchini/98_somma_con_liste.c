#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define BASE 10

lista_t * numToList (int, int);
lista_t * sommaListe (lista_t *, lista_t *);

int main (int argc, char * argv[])
{
	lista_t *head = NULL, *i1 = NULL, *i2 = NULL;
	int val, val2;

	scanf ("%d%d", &val, &val2);
	
	i1 = numToList (val, SEEK_END); /* Per come è fatta la funzione le devi dare le liste scritte al contrario, 190 è 0 9 1 */
	i2 = numToList (val2, SEEK_END);

	head = sommaListe (i1, i2);

	printl (head);

	i1 = empty (i1);
	i2 = empty (i2);
	head = empty (head);
	
	return 0;
}

lista_t * numToList (int val, int ordine)
{
	lista_t *head = NULL;
	int cifra;

	if (ordine) {
		while (val > 0) {
			cifra = val % BASE;
			head = append (head, cifra);
			val = val / BASE;
		}
	} else {
		while (val > 0) {
			cifra = val % BASE;
			head = push (head, cifra);
			val = val / BASE;
		}
	}

	return head;
}

lista_t * sommaListe (lista_t *n1, lista_t *n2)
{
	lista_t *result = NULL, *p1, *p2;
	int riporto, v1, v2, somma;

	riporto = 0;
	p1 = n1;
	p2 = n2;
	while (p1 || p2) {
		if (p1) {
			v1 = p1->val;
			p1 = p1->next;
		} else {
			v1 = 0;
		}

		if (p2) {
			v2 = p2->val;
			p2 = p2->next;
		} else {
			v2 = 0;
		}

		somma = v1 + v2 + riporto;
		result = push (result, (somma % BASE));
		riporto = somma / BASE;
	}

	if (riporto) {
		result = push (result, riporto);
	}

	return result;
}
