#include <stdio.h>
#define HMAX 16
#define BLOCKS '#'

int main(int argc, char * argv[])
{
	int h, i, c, ncanc, spazi;
	do {
		scanf("%d", &h);
	} while (h <= 0 || h > HMAX);
	spazi = h - 1;
	ncanc = 1;
	for (c = h; c > 0; c--) { /* Per ogni riga */
		for (i = 0; i < spazi; i++) { /* Nella prima riga bisogna mettere 15 spazi su una piramide da 16 */
			printf(" ");
		}
		for (i = 0; i < ncanc; i++) {
			printf("%c", BLOCKS);
		}
		printf("  ");
		for (i = 0; i < ncanc; i++) {
			printf("%c", BLOCKS);
		}
		printf("\n");
		spazi--; /* Alla riga successiva ci vorrà uno spazio in meno e un cancelletto in più */
		ncanc++;
	}

	return 0;
}
