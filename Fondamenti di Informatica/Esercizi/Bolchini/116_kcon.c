#include "list_int.h"
#define OCC 2
#define STOP 0

int kcon (lista_t*, int);
int knocon (lista_t*, int);

int main (int argc, char * argv[])
{
	int val, ris_kcon, ris_knocon;
	lista_t * head = NULL;

	scanf("%d", &val);
	while (val != STOP) {
		head = append (head, val);
		scanf("%d", &val);
	}
	scanf("%d", &val);

	ris_kcon = kcon (head, val);
	ris_knocon = knocon (head, val);
	printf ("kcon = %d\nknocon = %d\n", ris_kcon, ris_knocon);

	return 0;
}

int kcon (lista_t *head, int k)
{
	lista_t * ptr;
	int ris;
	
	ris = 0;
	ptr = head;
	
	if (ptr && ptr->next) {
		if (ptr->val == k && ptr->next->val == k) {
			ris = 1;
		} else {
			ptr = ptr->next;
			ris = kcon (ptr, k);
		}
	}

	return ris;
}

/*int knocon (lista_t *head, int k)
{
	lista_t * ptr;
	int ris;
	
	ris = 0;
	ptr = head;
	while (ptr) {
		if (ptr->val == k && ris < OCC) {
			if (ptr->next != NULL) {
				if (ptr->next->val != k) {
					ris++;
				}
			} else {
				ris++;
			}
		}
		ptr = ptr->next;
	}
	if (ris == OCC) {
		ris = 1;
	} else {
		ris = 0;
	}

	return ris;
}*/

int knocon (lista_t *head, int k)
{
	lista_t * ptr;
	int ris;
	
	ris = 0;
	ptr = head;
	while (ptr && !ris) {
		if (ptr->val == k && ptr->next) {
			while (ptr->next && !ris) {
				if (ptr->next->val == k) {
					ris = 1;
				}
				ptr = ptr->next;
			}
		}
		ptr = ptr->next;
	}

	return ris;
}